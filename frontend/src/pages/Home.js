/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from 'presentations/Divider';
import Typography from 'presentations/Typography';
import React, { Fragment } from "react";

const styles = ({ typography }) => ({
  root: {},
})

/**
 * Home Page component
 */
class Home extends React.Component {
  render() {
    const { classes } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          PRIME Backend End Training Program 4th Edition
          <Typography>Designed and structured by Agon Lohaj</Typography>
          <Divider />
        </Typography>
        <Typography variant={'p'}>
          Welcome to the forth training program by PRIME. This web app will be the container of all the materials that you need to have in order to follow through the training. We are going to publish every material that we use during our lectures, related assignments and also resources that you will need to complete the training successfully.
        </Typography>

        <Typography variant={'p'}>
          This is an interactive web app, and we build it in such a manner that is transparent to the participant following this. We are going to show code snippets, partial or complete code samples, examples, content and resources. We this interactive web app has some tricks up its sleeves (you’ll get to experience them very soon, “INSERT EVIL LAUGH”). The design guidelines of this web app are based on our own PRIME guidelines (you will get to use them often). The project setup contains all the relevant libraries that you are going to be using, however you may add more library references if necessary, although we believe that the ones provided will suffice. By the time you are reading this on your own laptop, you have already set it up (the instructions will be given soon, see “Getting Started” session)
        </Typography>

        <Typography variant={'p'}>
          The lectures will be taught by Agon Lohaj and they will take place at the offices at Prime in Pristina.
        </Typography>

        <Typography variant={'title'}>
          Msc. Agon Lohaj
        </Typography>
        <Typography variant={'p'}>
          A master graduate from the Technical University of Munich. Software Engineering Major, Computer Graphics and Vision Minor, Algorithms Minor. Chief Technical Officer (CTO) @PRIME. Tech enthusiast and a Drummer amongst other things!
        </Typography>

        <Typography variant={'p'}>
          The Backend training program is organized into 26 hours of lectures split into 2 hours per day, 2 times a week. The schedule of the training program is as follows:
          <ol>
            <li>Tuesdays 5:30pm until 7:30pm</li>
            <li>Thursdays 5:15pm until 7:15pm</li>
          </ol>
        </Typography>
        <Typography variant={'p'}>
          A person who completes the training successfully will be able to understand the following concepts:
          <ol>
            <li>Working with git, git tools, using GitLab as a repository with Sourcetree</li>
            <li>Working with CD/CI</li>
            <li>Working with Scala SBT</li>
            <li>Developing highly scalable and distributed applications with Play Framework in Java and Scala</li>
            <li>Working with MongoDB (i.e. storing, retrieving and manipulating data, optimizing performance, advanced aggregations etc.)</li>
            <li>Other supporting things like: Caching, Modeling, Testing, Security etc</li>
            <li>Agile Methodology</li>
          </ol>
        </Typography>
        <Typography variant={'p'}>
          Requirements:
          <ol>
            <li>Bachelor’s degree in computer science or related studies (BONUS)</li>
            <li>Basic coding skills</li>
            <li>A laptop</li>
            <li>English (BONUS)</li>
            <li>Motivation and Dedication</li>
          </ol>
        </Typography>
        <Typography variant={'p'}>
          In order to successfully complete the training program you will have to complete at least 40% of the assignments. That is 40% of the total points (points vary based on the difficulty of the assignment). All of the exercises/assignments involve Software Engineering as well as Algorithms (70/30 ratio).
        </Typography>
        <Typography variant={'p'}>
          At the end of this training program there will be 4-6 applicants who are going to be hired by Prime to continue their journey with us as full time interns to then further develop themselves! During this training think about what you would want to develop yourself into:
          <ol>
            <li>A data engineer</li>
            <li>A full-stack front-end/back-end engineer</li>
            <li>A quality assurance engineer</li>
            <li>A DevOps engineer</li>
          </ol>
          Pick your primary as well as a secondary preference. We are going to ask for this again at the end of the training and use it to conclude the program and make the hires!
        </Typography>
        <Typography variant={'p'}>
          As always, I generally ask for feedback as to how the training is going. Your suggestions, comments or feedback will be much appreciated. Furthermore, these feedback will help improve the program so that other participants in the future will have a better learning experience.
        </Typography>
        <Typography variant={'p'}>
          This program is an open source training, it is free and will be free to use. In case you decide to work with this, mentioning would be appreciated!
        </Typography>
        <Typography variant={'title'}>
          Contact:
        </Typography>
        <Typography variant={'p'}>
            Agon Lohaj: agonlohaj@goprime.io
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Home)
