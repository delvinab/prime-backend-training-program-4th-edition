/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import Code from "presentations/Code";
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import SimpleLink from "presentations/rows/SimpleLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Italic, Bold } from "presentations/Label";
import JavaConstructorVsMethodImage from 'assets/images/lecture2/constructor-vs-method-in-java.jpg'
import InheritanceLevelsImage from 'assets/images/lecture2/inheritance-levles.png'
import InheritanceVariantsImage from 'assets/images/lecture2/inheritance-variance.png'
import JavaTypePromotionImage from 'assets/images/lecture2/java-type-promotion.png'
import EncapsulationImage from 'assets/images/lecture2/encapsulation.png'
import AbstractClassImage from 'assets/images/lecture2/abstract-class-in-java.jpg'
import WhyUseInterfaceImage from 'assets/images/lecture2/why-use-java-interface.jpg'

const styles = ({ size, typography }) => ({
  root: {},
  image: {
    marginRight: size.spacing
  },
  horizontal: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    alignContent: 'flex-start',
    '& $vertical': {
      marginRight: size.spacing
    }
  },
  vertical: {
    display: 'flex',
    flexFlow: 'column wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start'
  },
})

const javaClassExampleCode = `class Car {
  String color; // field, instance variable
  int width, height; // field
  public void honk () { // method
    System.out.println("Honk!");
  }
}`

const javaBlockExampleCode = `public class Car {
  ...
  public static int nrCars; // field
  static { // a static block
    // initialize static variables
    // can also contain logic in here
    nrCars = 0;
  }
  ...
}`
const javaConstructor = `class Car {
  String color; // field, instance variable
  int width, height; // field
  public Car () {
    this.color = "Green";
    this.width = 50;
    this.height = 50;
  }
  public Car (String color, int width, int height) {
    this.color = color;
    this.width = width;
    this.height = height;
  }
  ...
}`

const javaInheritanceSyntax = `class Audi extends Car{
  // fields and methods
}`


const objectCreation = `new Calculation(); // anonymous or nameless object
Rectangle r1= new Rectangle(), r2 = new Rectangle(); // creating two objects at once
Rectangle clone = r1.clone();
Class rClass = Class.forName("Rectangle");
Rectangle s = (Rectangle) rClass.newInstance();
Car car = Factory.make();`

const aggregationCodeExample = `class Employee{  
  int id;  
  String name;  
  Address address; // Address is a class  
  ...  
}`

const overloadingCodeExample = `class Adder {  
  static int add (int a, int b) { return a + b; }
  static double add (double a, double b) { return a + b; }
  ...  
}`
const ambiguousOverloadingCodeExample = `class Adder {  
  static int add (int a, int b) { return a + b; }
  static double add (int a, int b) { return a + b; }
  ...  
}

Adder.add(11, 11); // ambiguity`

const methodOverridingCodeExample = `public class Programmer {
    public void code() {
        System.out.println("Coding in C++");
    }
}
public class JavaProgrammer extends Programmer{
    public void code() {
        System.out.println("Coding in Java");
    }
}

Programmer ben = new JavaProgrammer();
ben.code();`

const compositionCodeExample = `class StudentId {
    private String idNumber;//A-123456789
    private String bloodGroup;
    private String accountNumber;
}
public class Student {
    private String name;
    private int age;
    private int grade;
    private StudentId studentId;//Student HAS-A StudentId
    public void study() {
        System.out.println("Study");
    }
}`


const abstractClassExample = `abstract class Car {
  public void turn() {
    System.out.println("Turning");
  }
  public abstract void getMaxSpeed(); // max speed depends on who extends this class
}`

const interfaceExample = `interface Car {
  public void getMaxSpeed(); // max speed depends on who extends this class
}`
class JavaIntro extends React.Component {
  render() {
    const { classes, section } = this.props
    let javaClass = section.children[0]
    let javaObject = section.children[1]
    let inheritance = section.children[2]
    let polymorphism = section.children[3]
    let encapsulation = section.children[4]
    let abstraction = section.children[5]
    let objectCasting = section.children[6]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography>
          Lets start off with the definition of a class!
        </Typography>
        <Typography id={javaClass.id} variant={'title'}>
          {javaClass.display}
        </Typography>
        <Typography>
          Lets start off with the definition of a class! It is a template or blueprint from which objects are created. So in that sense it is a group of objects which have common properties.
        </Typography>
        <Typography>
          A class in Java can contain:
          <ol>
            <li>Fields</li>
            <li>Methods</li>
            <li>Constructors</li>
            <li>Blocks</li>
            <li>Nested class and interface</li>
          </ol>
          A variable which is created inside the class but outside the method is known as an <Bold>instance variable</Bold>. Instance variable doesn't get memory at compile time. It gets memory at runtime when an object or instance is created.
          <Code>
            {javaClassExampleCode}
          </Code>
        </Typography>
        <Typography>
          In Java, a <Bold>method</Bold> is like a function which is used to expose the behavior of an object.
        </Typography>
        <Typography>
          The <Bold>new</Bold> keyword is used to allocate memory at runtime. All objects get memory in Heap memory area.
          <Code>
            {`Car tesla = new Car();`}
          </Code>
        </Typography>
        <Typography variant="section">
          Constructor
        </Typography>
        <Typography>
          A constructor is a special method which has the same name as the class name. It is called once every time a new object or instance of the class is created. It is used (mainly) to initialize the object.
          There are the characteristics that a constructor has/must follow:
          <ol>
            <li>Constructor name must be the same as its class name</li>
            <li>A Constructor must have no explicit return type</li>
            <li>The constructor is invoked implicitly.</li>
            <li>The Java compiler provides a default constructor if you don't have any constructor in a class.</li>
            <li>A Java constructor cannot be abstract, static, final, and synchronized</li>
          </ol>
          There are two types of constructors:
          <ol>
            <li>Default Constructor</li>
            <li>Parametrized Constructor</li>
          </ol>
          Here are both examples:
          <Code>
            {javaConstructor}
          </Code>
          Constructors can also be used to perform other things like: object creation, starting a thread, calling a method, etc. You can perform any operation in the constructor as you perform in the method. A constructor returns the current class instance.
          Here is an image that explains the differences between a constructor and a normal method:
        </Typography>
        <img src={JavaConstructorVsMethodImage}/>
        <Typography id={javaObject.id} variant={'title'}>
          {javaObject.display}
        </Typography>
        <Typography>
          An entity that has state and behavior is known as an <Bold>object</Bold>. An object has three characteristics:
          <ol>
            <li>State: represents the data (value) of an object.</li>
            <li>Behavior: represents the behavior (functionality) of an object such as deposit, withdraw, etc.</li>
            <li>Identity: An object identity is typically implemented via a unique ID. The value of the ID is not visible to the external user. However, it is used internally by the JVM to identify each object uniquely.</li>
          </ol>
          For Example, Pen is an object. Its name is Reynolds; color is white, known as its state. It is used to write, so writing is its behavior.
        </Typography>
        <Typography>
          <Bold>An object is an instance of a class</Bold>. A class is a template or blueprint from which objects are created. So, an object is the instance (result) of a class.
          Definitions:
          <ol>
            <li>An object is a real-world entity.</li>
            <li>An object is a runtime entity.</li>
            <li>The object is an entity which has state and behavior.</li>
            <li>The object is an instance of a class.</li>
          </ol>
        </Typography>

        <Typography>
          These are the ways that you can create an object:
          <ol>
            <li>By new keyword</li>
            <li>By newInstance() method</li>
            <li>By clone() method</li>
            <li>By deserialization</li>
            <li>By factory method etc.</li>
          </ol>
        </Typography>

        <Typography>
          Here are some examples of different types of objects and how you can create them:
          <Code>
            {objectCreation}
          </Code>
        </Typography>
        <Typography id={inheritance.id} variant={'title'}>
          {inheritance.display}
        </Typography>
        <Typography variant={'section'}>
          Inheritance
        </Typography>
        <Typography>
          Inheritance in Java is a mechanism in which one object acquires all the properties and behaviors of a parent object. It is an important part of OOPs (Object Oriented programming system). The idea behind inheritance in Java is that you can create new classes that are built upon existing classes. When you inherit from an existing class, you can reuse methods and fields of the parent class. Moreover, you can add new methods and fields in your current class also.
        </Typography>
        <Typography>
          Inheritance represents the IS-A relationship which is also known as a parent-child relationship.
        </Typography>
        <Typography>
          Why use inheritance:
          <ol>
            <li>For Method Overriding (so runtime polymorphism can be achieved).</li>
            <li>For Code Reusability.</li>
          </ol>
          Syntax:
          <Code>
            {javaInheritanceSyntax}
          </Code>
          On the basis of class, there can be three types of inheritance in java: single, multilevel and hierarchical.<br/>
          In java programming, multiple and hybrid inheritance is supported through interface only.
        </Typography>
        <Typography>
          <img src={InheritanceLevelsImage} />
        </Typography>
        <Typography>
          <img src={InheritanceVariantsImage} />
        </Typography>
        <Typography variant={'section'}>
          Aggregation
        </Typography>
        <Typography>
          If a class have an entity reference, it is known as Aggregation. Aggregation represents HAS-A relationship.
          Example:
          <Code>
            {aggregationCodeExample}
          </Code>
          Why use aggregation:
          <ol>
            <li>Code reuse is also best achieved by aggregation when there is no is-a relationship.</li>
            <li>Inheritance should be used only if the relationship is-a is maintained throughout the lifetime of the objects involved; otherwise, aggregation is the best choice.</li>
          </ol>
        </Typography>
        <Typography variant={'section'}>
          Composition
        </Typography>
        <Typography>
          In this relationship, class B can not exist without class A – but class A can exist without class B. Here is an example that explains that:
          <Code>
            {compositionCodeExample}
          </Code>
          Student HAS-A StudentId. Student can exist without StudentId but StudentId can not exist without Student. This type of relationship is known as composition.
        </Typography>


        <Typography>
          Advantages of inheritance
          <ol>
            <li>Code reuse: the child class inherits all instance members of the parent class.</li>
            <li>You have more flexibility to change code: changing code in place is enough.</li>
            <li>You can use polymorphism: method overriding requires IS-A relationship.</li>
          </ol>
        </Typography>
        <Typography id={polymorphism.id} variant={'title'}>
          {polymorphism.display}
        </Typography>
        <Typography>
          Polymorphism is the ability of an object to take on many forms. Polymorphism in OOP occurs when a super class references a sub class object.<br/>
          All Java objects are considered to be polymorphic as they share more than one IS-A relationship (at least all objects will pass the IS-A test for their own type and for the class Object).
        </Typography>
        <Typography variant={'section'}>
          Method Overloading
        </Typography>
        <Typography>
          If a class has multiple methods having same name but different in parameters, it is known as Method Overloading.
          The Advantage using Method Overloading is that method overloading increases the readability of the program.<br/>
          There are two ways to overload the method in java
          <ol>
            <li>By changing number of arguments</li>
            <li>By changing the data type</li>
          </ol>
          In other words:
          <ol>
            <li>Must have a different parameter list.</li>
            <li>May have different return types.</li>
            <li>May have different access modifiers.</li>
            <li>May throw different exceptions.</li>
          </ol>
          <Code>
            {overloadingCodeExample}
          </Code>
          In java, method overloading is not possible by changing the return type of the method only because of ambiguity. Let's see how ambiguity may occur:
          <Code>
            {ambiguousOverloadingCodeExample}
          </Code>
          One type is promoted to another implicitly if no matching datatype is found. Let's understand the concept by the figure given below:
        </Typography>
        <Typography>
          <img src={JavaTypePromotionImage}/>
        </Typography>
        <Typography variant={'section'}>
          Method overriding
        </Typography>
        <Typography>
          If a subclass has the same method as declared in the super class, this is known as method overriding.
        </Typography>
        <Typography>
          Method overriding rules:
          <ol>
            <li>Must have the same parameter list.</li>
            <li>Must have the same return type: although a covariant return allows us to change the return type of the overridden method.</li>
            <li>Must not have a more restrictive access modifier: may have a less restrictive access modifier.</li>
            <li>Must not throw new or broader checked exceptions: may throw narrower checked exceptions and may throw any unchecked exception.</li>
            <li>Only inherited methods may be overridden (must have IS-A relationship).</li>
          </ol>
          <Code>
            {methodOverridingCodeExample}
          </Code>
        </Typography>
        <Typography id={encapsulation.id} variant={'title'}>
          {encapsulation.display}
        </Typography>
        <Typography>
          Encapsulation is achieved when each object keeps its state private, inside a class. Other objects don’t have direct access to this state. Instead, they can only call a list of public functions — called methods (behaviour).
        </Typography>
        <Typography>
          So, the object manages its own state via methods — and no other class can touch it unless explicitly allowed. If you want to communicate with the object, you should use the methods provided. But (by default), you can’t change the state.
        </Typography>
        <Typography>
          Let’s say we’re building a tiny Sims game. There are people and there is a cat. They communicate with each other. We want to apply encapsulation, so we encapsulate all “cat” logic into a Cat class. It may look like this:
          <img src={EncapsulationImage}/>
          <br/>
          Here the “state” of the cat is the private variables mood, hungry and energy. It also has a private method meow(). It can call it whenever it wants, the other classes can’t tell the cat when to meow.
        </Typography>
        <Typography>
          What they can do is defined in the public methods sleep(), play() and feed(). Each of them modifies the internal state somehow and may invoke meow(). Thus, the binding between the private state and public methods is made.
        </Typography>
        <Typography id={abstraction.id} variant={'title'}>
          {abstraction.display}
        </Typography>

        <Typography>
          In object-oriented design, programs are often extremely large. And separate objects communicate with each other a lot. So maintaining a large codebase like this for years — with changes along the way — is difficult.
          Abstraction is a concept aiming to ease this problem.
        </Typography>
        <Typography>
          This mechanism should hide internal implementation details. It should only reveal operations relevant for the other objects. In other wrods Abstraction is a process of hiding the implementation details and showing only functionality to the user.
        </Typography>
        <Typography>
          There are two ways to achieve abstraction in java
          <ol>
            <li>Abstract class</li>
            <li>Interface</li>
          </ol>
        </Typography>

        <Typography variant={'section'}>
          Abstract Class in Java
        </Typography>

        <Typography>
          Something to remember:
        </Typography>
        <div className={classes.horizontal}>
          <div className={classes.vertical}>
            <img src={AbstractClassImage} />
          </div>
          <div className={classes.vertical}>
            <ol>
              <li>An abstract class must be declared with an abstract keyword.</li>
              <li>It can have abstract and non-abstract methods.</li>
              <li>It cannot be instantiated.</li>
              <li>It can have constructors and static methods also.</li>
              <li>It can have final methods which will force the subclass not to change the body of the method.</li>
            </ol>
          </div>
        </div>
        <Typography>
          Example:
          <Code>
            {abstractClassExample}
          </Code>
        </Typography>
        <Typography variant={'section'}>
          Interface in Java
        </Typography>
        <Typography>
          An interface in Java is a blueprint of a class. It has static constants and abstract methods.
        </Typography>
        <Typography>
          The interface in Java is a mechanism to achieve abstraction. There can be only abstract methods in the Java interface, not method body. It is used to achieve abstraction and multiple inheritance in Java.
        </Typography>
        <Typography>
          Java Interface also represents the IS-A relationship.<br/>
          It cannot be instantiated just like the abstract class.<br/>
          Since Java 8, we can have default and static methods in an interface.<br/>
          Since Java 9, we can have private methods in an interface.
        </Typography>

        <Typography>
          Interfaces are used to:
        </Typography>

        <div className={classes.horizontal}>
          <div className={classes.vertical}>
            <img src={WhyUseInterfaceImage}/>
          </div>
          <div className={classes.vertical}>
            <ol>
              <li>It is used to achieve abstraction.</li>
              <li>By interface, we can support the functionality of multiple inheritance.</li>
              <li>It can be used to achieve loose coupling.</li>
            </ol>
          </div>
        </div>
        <Typography>
          An interface is declared by using the interface keyword. It provides total abstraction; means all the methods in an interface are declared with the empty body, and all the fields are public, static and final by default. A class that implements an interface must implement all the methods declared in the interface.
        </Typography>
        <Typography>
          Example:
          <Code>
            {interfaceExample}
          </Code>
        </Typography>

        <Typography fontStyle={'italic'}>
          If you want to know more about the topics in this section follow:
          <ul>
            <li><SimpleLink href="https://www.javatpoint.com/object-and-class-in-java">https://www.javatpoint.com/object-and-class-in-java</SimpleLink></li>
            <li><SimpleLink href="https://www.freecodecamp.org/news/java-object-oriented-programming-system-principles-oops-concepts-for-beginners/">https://www.freecodecamp.org/news/java-object-oriented-programming-system-principles-oops-concepts-for-beginners/</SimpleLink></li>
            <li><SimpleLink href="https://docs.oracle.com/javase/tutorial/">https://docs.oracle.com/javase/tutorial/</SimpleLink></li>
          </ul>
        </Typography>
        <Typography fontStyle={'italic'}>
          If you want to know more about the differences between an interface and an abstract class follow this link
          <ul>
            <li><SimpleLink href="https://www.javatpoint.com/difference-between-abstract-class-and-interface">https://www.javatpoint.com/difference-between-abstract-class-and-interface</SimpleLink></li>
          </ul>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(JavaIntro)
