/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import Code from "presentations/Code";
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import SimpleLink from "presentations/rows/SimpleLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Italic, Bold } from "presentations/Label";

const styles = ({ typography }) => ({
  root: {},
})

const functionalInterface = `interface Ball
{
 void hit();
}`

const lambdaExpression = `() -> { System.out.println("You hit it!");}`


const anotherFunctionalInterface = `interface FuncInterface { 
    void abstractFun(int x);  // An abstract function 
    // A non-abstract (or default) function 
    default void normalFun() { 
       System.out.println("Hello"); 
    }
};
FuncInterface fobj = (x)->System.out.println(2 * x);`

const statementLambdaExpression = `interface MyString {
	String myStringFunction(String str);
}

MyString reverse = (str) -> {
  String result = "";
  
  for(int i = str.length()-1; i >= 0; i--)
    result += str.charAt(i);
  
  return result;
};
System.out.println(reverse.myStringFunction("Lambda Demo"));`


const streamingExamples = `List<Integer> integers = Arrays.asList(1, 4, 5);
boolean anyMatch = integers.stream().anyMatch((number) -> number >= 50); // is one of the elements greater than 50
List<Integer> greaterThanFifty = integers.stream().filter((number) -> number >= 50).collect(Collectors.toList()); // all those greater than 50
boolean areAllPositive = integers.stream().allMatch((number) -> number >= 0); // are all the members positive
boolean areAllNoneNegative = integers.stream().noneMatch((number) -> number < 0); // are all the members not negative

Optional<Integer> optionalInteger = integers.stream().filter((number) -> number >= 50).findFirst(); // the first one greater than 50
if (optionalInteger.isPresent()) {
  Integer greaterThan50 = optionalInteger.get();
}
Integer findWithDefaultValue = integers.stream().filter((number) -> number >= 50).findFirst().orElse(-1); // all those greater than 50
List<Double> toThePowerOfTwo = integers.stream().map((number) -> Math.pow(number, 2)).collect(Collectors.toList()); // all those greater than 50

Integer sum = integers.stream().reduce(0, (total, next) -> total + next); // A simple SUM
Integer max = integers.stream().reduce(0, (total, next) -> Math.max(total, next)); // the max number
Integer anotherMax = integers.stream().reduce(0, Math::max); // Same as the above`

const filterMapReduceExample = `Double chaining = integers
      .stream()
      .filter(next -> next >= 0)
      .map(next -> Math.pow(next, 2))
      .reduce(0.0, (total, next) -> total + next);`

const filterMapReduceInParallelExample = `Long count = integers
    .parallelStream()
    .skip(1)
    .filter(next -> next >= 0)
    .map(next -> Math.pow(next, 2))
    .count();`


const constructStreamFromArray = `String[] arr = new String[]{"a", "b", "c"};
Stream<String> streamOfArrayFull = Arrays.stream(arr);`

const constructingStreamFromCollection = `Collection<String> collection = Arrays.asList("a", "b", "c");
Stream<String> streamOfCollection = collection.stream();`

const concatination = `Streams.concat(integers.stream(), integers.stream()).forEach((next) -> Logger.of(this.getClass()).debug("next {}", next));`

const generateTenItems = `Stream<String> streamGenerated = Stream.generate(() -> "element").limit(10);`


const completableFuturesExample = `CompletableFuture<String> completableFuture = new CompletableFuture<>(); // Its a calculation that will be completed in the future, that is a string will be returned sooner or later
completableFuture.complete("Agon"); // Manually Complete it
String result = completableFuture.get(); // Get the result. From Asynchron to Syncron, back to main thread, wait until completable future finishes, throws ExecutionException, InterruptedException`

const aSimpleRunAsync = `CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(new Runnable() {
  @Override
  public void run() {
    Logger.of(this.getClass()).debug("Running Asynchronously");
  }
});
CompletableFuture.runAsync(() -> Logger.of(this.getClass()).debug("Running Asynchronously"));`

const supplyAndApply = `// Run a task specified by a Supplier object asynchronously
CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> "Agon");
CompletableFuture<String> greetingFuture = future.thenApply(name -> "Hello " + name);`

const allOfCompletableFuture = `CompletableFuture<String> heavyCalc1 = CompletableFuture.supplyAsync(() -> "Going to work hard to get a string!");
CompletableFuture<String> heavyCalc2 = CompletableFuture.supplyAsync(() -> "Going to do a lot of data processing here");
CompletableFuture<String> heavyCalc3 = CompletableFuture.supplyAsync(() -> "Going to process some user requests");

CompletableFuture<String> results = CompletableFuture.allOf(heavyCalc1, heavyCalc2, heavyCalc3).thenApply((empty) -> {
  Logger.of(this.getClass()).debug("I don't execute until all of the three heavy calcs are done");
  String result1 = heavyCalc1.getNow("");
  String result2 = heavyCalc2.getNow("");
  String result3 = heavyCalc3.getNow("");
  return String.format("Result %s %s %s", result1, result2, result3);
}).thenCompose((result) -> {
  Logger.of(this.getClass()).debug("Finished with {}", result);
  return CompletableFuture.completedFuture(result);
});`

const raceCondition = `CompletableFuture<String> cacheAPI = CompletableFuture.supplyAsync(() -> {
  try {
    Thread.sleep(1000);
  } catch (InterruptedException e) {
    e.printStackTrace();
  }
  return "Caches Response!";
});
CompletableFuture<String> serverAPI = CompletableFuture.supplyAsync(() -> {
  try {
    Thread.sleep(2000);
  } catch (InterruptedException e) {
    e.printStackTrace();
  }
  return "Server Response";
});

CompletableFuture.anyOf(cacheAPI, serverAPI).thenApply((empty) -> {
  Logger.of(this.getClass()).debug("I complete if any of the above finish successfully, don't care");
  return serverAPI.getNow(cacheAPI.getNow("Not Found"));
}).thenApply((result) -> {
  Logger.of(this.getClass()).debug("Finished with {}", result);
  return result;
});`

const errorHandling = `cacheAPI.thenCompose((result) -> {
  if (Strings.isNullOrEmpty(result)) {
    throw new CompletionException(new IllegalArgumentException("I cannot work with this"));
  }
  return CompletableFuture.completedFuture("Greeting " + result);
}).handle((res, ex) -> {
  if (ex != null) {
    Logger.of(this.getClass()).debug("Finished with {}", ex.getMessage());
    return "Unknown!";
  }
  return res;
}).thenApply((result) -> {
  // I still continue as nothing happened
  Logger.of(this.getClass()).debug("Finished with {}", result);
  return result;
}).exceptionally((ex) -> {
  // I don't catch the above error since it was handlded
  Logger.of(this.getClass()).debug("Will I get called?");
  return "Uknown!";
});`

const chainingExample = `CompletableFuture.supplyAsync(() -> {
	// Code which might throw an exception
	return "Some result";
}).thenApply(result -> {
	return "processed result";
}).thenApply(result -> {
	return "result after further processing";
}).thenAccept(result -> {
	// do something with the final result
});`

const reducerMethodVariations = `// When the return value is of the same data type as the input, than you can do something like this:
// Using only accumulator
Optional<Integer> optionalSum = integers.stream().reduce((total, next) -> total + next); // I return the sum, or cannot do it if there are no elements
// Using identity -> 0, and accumulator
Integer sum = integers.stream().reduce(0, (total, next) -> total + next); // I started with 0, so I return 0 if there are no elements

// whereas if the return data type is something else then the elements in the stream you need to provide a combiner
List<Integer> doubleTheSize = integers.stream().reduce(
  new ArrayList<Integer>(), // identity -> starting point
  (accumulator, next) -> { // accumulator function
    accumulator.add(next);
    accumulator.add(next);
    return accumulator;
  }, (a, b) -> { // combiner
    a.addAll(b);
    return a;
  });`

const findFirstExample = `Optional<Integer> optionalFirst = integers.stream().findFirst();
boolean isPresent = optionalFirst.isPresent();
Integer getOrDefault = optionalFirst.orElse(-1);
Integer first = optionalFirst.get(); // getting the value without checking is present, uuuu I wouldn't do that, dangerous`

const collectorsToListAndSet = `List<String> collectorCollection = 
productList.stream().map(Product::getName).collect(Collectors.toList());`

const collectorsToString = `String listToString = productList.stream().map(Product::getName)
.collect(Collectors.joining(", ", "[", "]"));`

const averageValueCollector = `double averagePrice = productList.stream()
  .collect(Collectors.averagingInt(Product::getPrice));`

const groupingCollector = `Map<Integer, List<Product>> collectorMapOfLists = productList.stream()
.collect(Collectors.groupingBy(Product::getPrice));`

const customCollector = `List<Integer> collected = integers.stream().collect(
Collector.of(
  ArrayList::new, // supplier
  (list, element) -> list.add(element), // accumulator
  (first, second) -> { // combiner
    first.addAll(second);
    return first;
  })
);`
class JavaReleases extends React.Component {
  render() {
    const { classes, section } = this.props
    let lambda = section.children[0]
    let streaming = section.children[1]
    let completableFutures = section.children[2]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography variant={'p'}>
          In this section we are going to explain some more features that were introduced on the Java version 8 and above.
        </Typography>

        <Typography id={lambda.id} variant={'title'}>
          {lambda.display}
        </Typography>
        <Typography>
          Lambda expressions basically express instances of functional interfaces (An interface with single abstract method is called functional interface. An example is java.lang.Runnable). lambda expressions implement the only abstract function and therefore implement functional interfaces
        </Typography>
        <Typography>
          Lambda expressions are added in Java 8 and provide below functionalities:
          <ol>
            <li>Enable to treat functionality as a method argument, or code as data.</li>
            <li>A function that can be created without belonging to any class.</li>
            <li>A lambda expression can be passed around as if it was an object and executed on demand.</li>
          </ol>
        </Typography>

        <Typography>
          The general syntax for a lambda expression is this:
          <Code>
            (parameters) -> expression
          </Code>
          or
          <Code>
            {`(parameters) -> { statement; ... }`}
          </Code>
          If you use an expression, a semicolon is not required. If you use one or more statements, the statements must be enclosed in curly braces and a semicolon is required at the end of each statement.
        </Typography>
        <Typography>
          Example, lets look at the following interface:
          <Code>
            {functionalInterface}
          </Code>
          Here the only abstract method is the hit method.
        </Typography>
        <Typography>
          A functional interface can contain additional methods, provided they are not abstract. Until Java 8, this was not possible because an interface could contain only abstract methods. However, in Java 8 you can create default methods which provide a default implementation. Thus a functional interface can contain one or more default methods, but can contain only one abstract method.
        </Typography>
        <Typography>
          A lambda expression is a concise way to create an anonymous class that implements a functional interface. Instead of providing a formal method declaration that includes the return type, method name, parameter types, and method body, you simply define the parameter types and the method body. The Java compiler infers the rest based on the context in which you use the lambda expression.
        </Typography>
        <Typography>
          The parameter types are separated from the method body by a new operator, called the arrow operator, which consists of a hyphen followed by a greater-than symbol. Here’s an example that implements the Ball interface:
          <Code>
            {lambdaExpression}
          </Code>
          Another example:
          <Code>
            {anotherFunctionalInterface}
          </Code>
          Another example for statements:
          <Code>
            {statementLambdaExpression}
          </Code>
        </Typography>
        <Typography id={streaming.id} variant={'title'}>
          {streaming.display}
        </Typography>
        <Typography>
          Streaming API allows us to manipulate Arrays, Lists, Collections and do chained data manipulation. Combined with lambda expressions we can do so many operations with few lines of code.
        </Typography>
        <Typography>
          Here are some examples:
          <Code>
            {streamingExamples}
          </Code>
          Following the principles between filter, map and reduce you can construct pipelines of data processing with ease
          <Code>
            {filterMapReduceExample}
          </Code>
          and also run them in parallel quite easily:
          <Code>
            {filterMapReduceInParallelExample}
          </Code>
        </Typography>
        <Typography>
          Initialising stream from array:
          <Code>
            {constructStreamFromArray}
          </Code>
          But also from collection
          <Code>
            {constructingStreamFromCollection}
          </Code>
          Generating Items:
          <Code>
            {generateTenItems}
          </Code>
          Concatinating two streams:
          <Code>
            {concatination}
          </Code>
        </Typography>
        <Typography>
          To perform a sequence of operations over the elements of the data source and aggregate their results, three parts are needed – <Bold>the source</Bold>, <Bold>intermediate operation(s)</Bold> and a <Bold>terminal operation</Bold>.
        </Typography>
        <Typography>
          Intermediate operations return a new modified stream. For example, to create a new stream of the existing one without few elements the skip() method should be used. So keep this in mind that any intermediate operation will create a <Bold>new</Bold> stream based off of the existing one.
        </Typography>
        <Typography>
          The terminal operations of the stream are functions that sink the results to an end. Example:
          <ol>
            <li>count</li>
            <li>collect</li>
            <li>reduce</li>
            <li>...</li>
          </ol>
          Also streams are executed only when such terminal functions exists, otherwise why bother executing?
        </Typography>
        <Typography>
          Here are some methods and definitions:
          <ol>
            <li><Bold>filter</Bold> a function that given the evaluation (true or false) keeps or filters out (discards) the element under test. Example:
              <Code>items.stream().filter(next -> next.equals("Treasure")) // Looking only for treasures</Code>
              Where items is a List{`<String>`}
            </li>
            <li><Bold>map</Bold> a function which maps every value to the new returned value within the expression. Example:
              <Code>integers.stream().map(next -> Math.pow(next, 2)) // Map x like this f(x) => x^2</Code>
              So in other words its like a mathematics function but can be used for anything, like getting all the emails from your users
              <Code>users.stream().map(next -> next.getEmail()) // Map x like this f(x) => x.email</Code>
              So now we went from a List{`<User>`} to a List{`<String>`}
            </li>
            <li><Bold>sorted</Bold> a function which sorts the elements of the stream based on a Comparator. Example:
              <Code>integers.stream().sorted((a, b) -> b - a) // Sorted such that the elements are ordered descending</Code>
              Depending on the return value of the method the following is done:
              <ol>
                <li>Negative: indicates that value a is smaller than b</li>
                <li>0: indicates that value a is equal to b</li>
                <li>Positive: indicates that value a is greater to b</li>
              </ol>
              Sorting can also be done on object properties like this:
              <Code>users.stream().sorted((a, b) -> b.getTotalMoney() - a.getTotalMoney()) // From the richest to the poorest</Code>
            </li>
            <li><Bold>findFirst</Bold> finds the first item within the list, if it exists. That's why the return value of this function is an Optional{`<T>`} (Terminal Operation). Example:
              <Code>
                {findFirstExample}
              </Code>
            </li>
            <li><Bold>findAny</Bold> same as findFirst but selects any arbitrary element within the stream, that is its not deterministic (Keep in mind that streams can be run in parallel) (Terminal Operation)</li>
            <li><Bold>limit</Bold> limits the number of results to x</li>
            <li><Bold>skip</Bold> skips x results</li>
            <li><Bold>count</Bold> returns the count of the elements in the stream (Terminal Operation)</li>
            <li><Bold>reduce</Bold> There are three variations of this method, which differ by their signatures and returning types. They can have the following parameters: (Terminal Operation)
              <ol>
                <li><Bold>identity</Bold>: the initial value for an accumulator or a default value if a stream is empty and there is nothing to accumulate</li>

                <li><Bold>accumulator</Bold>: a function which specifies a logic of aggregation of elements. As accumulator creates a new value for every step of reducing, the quantity of new values equals to the stream's size and only the last value is useful. This is not very good for the performance.</li>
                <li><Bold>combiner</Bold>: a function which aggregates results of the accumulator. Combiner is called only in a parallel mode to reduce results of accumulators from different threads.</li>
              </ol>
              Example:
              <Code>
                {reducerMethodVariations}
              </Code>
            </li>
            <li><Bold>collect</Bold> Reduction of a stream can also be executed by another terminal operation – the collect() method. It accepts an argument of the type Collector, which specifies the mechanism of reduction. There are already created predefined collectors for most common operations. They can be accessed with the help of the Collectors type (Terminal Operation). Example
              <ol>
                <li>Converting a stream to the Collection (Collection, List or Set):
                  <Code>
                    {collectorsToListAndSet}
                  </Code>
                </li>
                <li>Reducing to String:
                  <Code>
                    {collectorsToString}
                  </Code>
                </li>
                <li>Processing the average value of all numeric elements of the stream:
                  <Code>
                    {averageValueCollector}
                  </Code>
                </li>
                <li>Grouping of stream’s elements according to the specified function:
                  <Code>
                    {groupingCollector}
                  </Code>
                </li>
                <li>and more but also you can create your own collectors using the method <Italic>of()</Italic> of the type <Italic>Collector</Italic>, like this:
                <Code>{customCollector}</Code></li>
              </ol>
            </li>
          </ol>
        </Typography>
        <Typography id={completableFutures.id} variant={'title'}>
          {completableFutures.display}
        </Typography>
        <Typography>
          CompletableFuture is used for asynchronous programming in Java. Asynchronous programming is a means of writing non-blocking code by running a task on a separate thread than the main application thread and notifying the main thread about its progress, completion or failure.
        </Typography>
        <Typography>
          This way, your main thread does not block/wait for the completion of the task and it can execute other tasks in parallel. Having this kind of parallelism greatly improves the performance of your programs.
        </Typography>
        <Typography>
          CompletableFuture is an extension to Java’s Future API which was introduced in Java 5.
        </Typography>
        <Typography>
          A Future is used as a reference to the result of an asynchronous computation. It provides an isDone() method to check whether the computation is done or not, and a get() method to retrieve the result of the computation when it is done.
        </Typography>
        <Typography>
          Limitations of Future
          <ol>
            <li><Bold>It cannot be manually completed</Bold>:<br/>
              Let’s say that you’ve written a function to fetch the latest price of an e-commerce product from a remote API.
              Since this API call is time-consuming, you’re running it in a separate thread and returning a Future from your function.<br/>
              Now, let’s say that If the remote API service is down, then you want to complete the Future manually by the last cached price of the product.<br/>
              Can you do this with Future? No!</li>
            <li><Bold>You cannot perform further action on a Future’s result without blocking</Bold>:<br/>
              Future does not notify you of its completion. It provides a get() method which blocks until the result is available.<br/>
              You don’t have the ability to attach a callback function to the Future and have it get called automatically when the Future’s result is available.</li>
            <li><Bold>Multiple Futures cannot be chained together</Bold>:<br/>Sometimes you need to execute a long-running computation and when the computation is done, you need to send its result to another long-running computation, and so on.</li>
            <li><Bold>You can not combine multiple Futures together:</Bold><br/>Let’s say that you have 10 different Futures that you want to run in parallel and then run some function after all of them completes. You can’t do this as well with Future.</li>
            <li><Bold>No Exception Handling</Bold>:<br/>Future API does not have any exception handling construct.</li>
          </ol>
          So many limitations right? Well, That’s why we have CompletableFuture. You can achieve all of the above with CompletableFuture.
        </Typography>
        <Typography>
          In order to get right to completable futures lets show some examples:
          <Code>
            {completableFuturesExample}
          </Code>
          Running Async without return:
          <Code>
            {aSimpleRunAsync}
          </Code>
          Supply Async and Apply Async:
          <Code>
            {supplyAndApply}
          </Code>
          Chaining
          <Code>
            {chainingExample}
          </Code>
          All of Completable Futures, when all finish continue on the next one:
          <Code>
            {allOfCompletableFuture}
          </Code>
          A race condition
          <Code>
            {raceCondition}
          </Code>
          Error Handling
          <Code>
            {errorHandling}
          </Code>
        </Typography>
        <Typography fontStyle={'italic'}>
          If you want to know more about the topics in this section follow:
          <ol>
            <li><SimpleLink href="https://www.dummies.com/programming/java/how-to-use-lambda-expressions-in-java-8/">https://www.dummies.com/programming/java/how-to-use-lambda-expressions-in-java-8/</SimpleLink></li>
            <li><SimpleLink href="https://www.geeksforgeeks.org/lambda-expressions-java-8/">https://www.geeksforgeeks.org/lambda-expressions-java-8/</SimpleLink></li>
            <li><SimpleLink href="https://www.baeldung.com/java-8-streams">https://www.baeldung.com/java-8-streams</SimpleLink></li>
            <li><SimpleLink href="https://www.baeldung.com/java-8-new-features">https://www.baeldung.com/java-8-new-features</SimpleLink></li>
            <li><SimpleLink href="https://www.callicoder.com/java-8-completablefuture-tutorial/">https://www.callicoder.com/java-8-completablefuture-tutorial/</SimpleLink></li>
          </ol>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(JavaReleases)
