/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import JavaIntro from "pages/lecture2/JavaIntro";
import OOP from "pages/lecture2/OOP";
import OOPMisc from "pages/lecture2/OOPMisc";
import JavaReleases from "pages/lecture2/JavaReleases";
import Assignments from "pages/lecture2/Assignments";
import Intro from "pages/lecture2/Intro";
import React from "react";
const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_2.JAVA_INTRODUCTION:
        return <JavaIntro {...props} />
      case PAGES.LECTURE_2.OOP:
        return <OOP {...props} />
      case PAGES.LECTURE_2.OOP_MISC:
        return <OOPMisc {...props} />
      case PAGES.LECTURE_2.JAVA_RELEASES:
        return <JavaReleases {...props} />
      case PAGES.LECTURE_2.ASSIGNMENTS:
        return <Assignments {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
