/**
 * Created by Agon Lohaj on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture8/Intro";
import AkkaCluster from "pages/lecture8/AkkaCluster";
import AkkaPubSub from "pages/lecture8/AkkaPubSub";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_8.AKKA_CLUSTER:
        return <AkkaCluster {...props} />
      case PAGES.LECTURE_8.AKKA_PUB_SUB:
        return <AkkaPubSub {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
