/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import IndexingExample from 'assets/images/lecture10/index-for-sort.bakedsvg.svg'
import SimpleLink from "presentations/rows/SimpleLink";
import Code from "presentations/Code";
import { Bold, Highlighted } from "presentations/Label";

const styles = ({ size, typography }) => ({
  root: {
  }
})

const simpleAscendingIndex = `collection.createIndex(Indexes.ascending("name"))
.subscribe(new PrintToStringSubscriber<String>());`

const simpleDescendingIndex = `collection.createIndex(Indexes.descending("name"))
.subscribe(new PrintToStringSubscriber<String>());`

const compoundAscendingIndex = `collection.createIndex(Indexes.ascending("stars", "name"))
.subscribe(new PrintToStringSubscriber<String>());`

const compoundDescendingIndex = `collection.createIndex(Indexes.descending("stars", "name"))
.subscribe(new PrintToStringSubscriber<String>());`

const textIndex = `collection.createIndex(Indexes.text("name")).subscribe(new PrintToStringSubscriber<String>());`

const hashedIndex = `collection.createIndex(Indexes.hashed("_id")).subscribe(new PrintToStringSubscriber<String>());`

const twoDSphere = `collection.createIndex(Indexes.geo2dsphere("contact.location")).subscribe(new PrintToStringSubscriber<String>());`

const geoHayStackExample = `IndexOptions haystackOption = new IndexOptions().bucketSize(1.0);
collection.createIndex(
            Indexes.geoHaystack("contact.location", Indexes.ascending("stars")),
            haystackOption)
        .subscribe(new PrintToStringSubscriber<String>());`


const listIndexesExample = `collection.listIndexes().subscribe(new PrintDocumentSubscriber());`

const routes = `GET            /api/lecture10/index                       @io.training.api.controllers.LectureTenController.index()
POST           /api/lecture10/index                       @io.training.api.controllers.LectureTenController.createIndexes()
DELETE         /api/lecture10/index                       @io.training.api.controllers.LectureTenController.dropIndexes()`


const artillery = `npm install artillery -g
artillery quick --count 20 --num 400 http://localhost:9000/api/lecture10/index`
const Indexing = (props) => {
  const { classes, section } = props
  const objectId = section.children[0]
  const asvDescIndexes = section.children[1]
  const indexTypes = section.children[2]
  const examples = section.children[3]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 10: {section.display}
        <Typography>
          <SimpleLink href="https://docs.mongodb.com/manual/indexes/">https://docs.mongodb.com/manual/indexes/</SimpleLink>
        </Typography>
        <Divider />
      </Typography>
      <Typography>
        Indexes support the efficient execution of queries in MongoDB. Without indexes, MongoDB must perform a collection scan, i.e. scan every document in a collection, to select those documents that match the query statement. If an appropriate index exists for a query, MongoDB can use the index to limit the number of documents it must inspect.
      </Typography>

      <Typography>
        Indexes are special data structures (MongoDB indexes use a B-tree data structure.) that store a small portion of the collection’s data set in an easy to traverse form. The index stores the value of a specific field or set of fields, ordered by the value of the field. The ordering of the index entries supports efficient equality matches and range-based query operations. In addition, MongoDB can return sorted results by using the ordering in the index.
      </Typography>
      <Typography>
        The following diagram illustrates a query that selects and orders the matching documents using an index:
      </Typography>
      <img src={IndexingExample} />
      <Typography>
        Fundamentally, indexes in MongoDB are similar to indexes in other database systems. MongoDB defines indexes at the collection level and supports indexes on any field or sub-field of the documents in a MongoDB collection.
      </Typography>

      <Typography id={objectId.id} variant={'title'}>
        {objectId.display}
      </Typography>
      <Typography>
        MongoDB creates a unique index on the _id field during the creation of a collection. The _id index prevents clients from inserting two documents with the same value for the _id field. You cannot drop this index on the _id field.
      </Typography>
      <Typography>
        To create an index using the Java driver, use com.mongodb.client.MongoCollection.createIndex.
      </Typography>
      <Typography id={asvDescIndexes.id} variant={'title'}>
        {asvDescIndexes.display}
      </Typography>
      <Typography>
        To create a specification for an ascending index, use the <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-core/com/mongodb/client/model/Indexes.html">Indexes.ascending</SimpleLink> static helper methods.
      </Typography>
      <Typography variant="section">
        Single field Index
      </Typography>
      <Typography>
        The following example creates an ascending index on the name field:
        <Code>
          {simpleAscendingIndex}
        </Code>
        Or for descending:
        <Code>
          {simpleDescendingIndex}
        </Code>
      </Typography>
      <Typography variant="section">
        Compound Index
      </Typography>
      <Typography>
        The following example creates an ascending compound index on the stars field and the name field:
        <Code>
          {compoundAscendingIndex}
        </Code>
        Or descending:
        <Code>
          {compoundDescendingIndex}
        </Code>
      </Typography>

      <Typography id={indexTypes.id} variant={'title'}>
        {indexTypes.display}
      </Typography>
      <Typography>
        Here is a list of supported indexes:

        <ol>
          <li><Bold>Text Indexes</Bold><br/>
            MongoDB provides <SimpleLink href="https://docs.mongodb.com/manual/core/index-text/">text indexes</SimpleLink> to support text search of string content. Text indexes can include any field whose value is a string or an array of string elements. To create a specification for a text index, use the <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-core/com/mongodb/client/model/Indexes.html#text(java.lang.String)">Indexes.text</SimpleLink> static helper method.
          <Code>
            {textIndex}
          </Code></li>
          <li><Bold>Hashed Indexes</Bold><br/>
            To create a specification for a <SimpleLink href="https://docs.mongodb.com/manual/core/index-hashed/">hashed index</SimpleLink> index, use the <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-core/com/mongodb/client/model/Indexes.html#hashed(java.lang.String)">Indexes.hashed</SimpleLink> static helper method.
            <Code>
              {hashedIndex}
            </Code></li>
          <li><Bold>Geospatial Indexes</Bold><br/>
            To support geospatial queries, MongoDB supports various geospatial indexes.<br/>
            <Bold>2dsphere</Bold><br/>
            To create a specification for a <SimpleLink href="https://docs.mongodb.com/manual/core/2dsphere/">2dsphere index</SimpleLink>, use the <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-core/com/mongodb/client/model/Indexes.html#geo2dsphere(java.lang.String...)">Indexes.geo2dsphere</SimpleLink> static helper methods.
            <Code>
              {twoDSphere}
            </Code>
            <Bold>geoHaystack</Bold><br/>
            To create a specification for a <SimpleLink href="https://docs.mongodb.com/manual/core/geohaystack/">geoHaystack</SimpleLink> index, use the <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-core/com/mongodb/client/model/Indexes.html#geoHaystack(java.lang.String,org.bson.conversions.Bson)">Indexes.geoHaystack</SimpleLink> method. geoHaystack indexes can improve performance on queries that use flat geometries.
            <Code>
              {geoHayStackExample}
            </Code>
          </li>
        </ol>
        Use the <Highlighted>listIndexes()</Highlighted> method to get a list of indexes. The following lists the indexes on the collection:
        <Code>
          {listIndexesExample}
        </Code>
        For more information follow:
        <ol>
          <li><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/driver-reactive/tutorials/indexes/">https://mongodb.github.io/mongo-java-driver/4.1/driver-reactive/tutorials/indexes/</SimpleLink></li>
          <li><SimpleLink href="https://docs.mongodb.com/manual/indexes/">https://docs.mongodb.com/manual/indexes/</SimpleLink></li>
          <li><SimpleLink href="https://docs.mongodb.com/manual/core/index-properties/">https://docs.mongodb.com/manual/core/index-properties/</SimpleLink></li>
        </ol>
      </Typography>

      <Typography id={examples.id} variant={'title'}>
        {examples.display}
      </Typography>
      <Typography>
        Let's look an example on what differences does it make when using indexes or not. For this we have a method that creates an index first, and then queries and another one which doesn't do that. At our api these are listed over here:
        <Code>
          {routes}
        </Code>
        In order to bombard and benchmnark our requests, we are going to use an open source library called <SimpleLink href="https://www.npmjs.com/package/artillery">artillery</SimpleLink>:
        <Code>
          {artillery}
        </Code>
        Can you see the difference?
      </Typography>
      <Typography>

      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(Indexing)
