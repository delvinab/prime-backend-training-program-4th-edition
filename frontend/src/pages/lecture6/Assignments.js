/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import faker from 'faker'
import _ from 'underscore'
import withTests from 'middleware/withTests'
import { Button, Table, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import LoadingIndicator from "presentations/LoadingIndicator";
import ErrorBox from "presentations/ErrorBox";
import Code from "presentations/Code";
import { Bold } from "presentations/Label";

const routes = `POST           /api/assignments/lecture6/users/setup                              @io.training.api.controllers.AssignmentLectureSixController.setup(request: Request)
GET            /api/assignments/lecture6/users                                    @io.training.api.controllers.AssignmentLectureSixController.all(request: Request)
POST           /api/assignments/lecture6/users                                    @io.training.api.controllers.AssignmentLectureSixController.save(request: Request)
PUT            /api/assignments/lecture6/users/:id                                @io.training.api.controllers.AssignmentLectureSixController.update(request: Request, id: String)
DELETE         /api/assignments/lecture6/users/:id                                @io.training.api.controllers.AssignmentLectureSixController.delete(request: Request, id: String)`

const styles = ({ typography, size }) => ({
  setup: {
    position: 'relative',
    width: '100%',
    height: 80
  }
})

const rowStyles = ({ typography, size }) => ({
  code: {
    height: 300
  },
  set: {
    color: 'green',
  },
  error: {
    color: 'red'
  }
})

const model = `{
  id: 'my id', 
  type: 'user type',
  gender: 'M or F',
  age: 0 - 90,
  name: 'a name',
  username: 'user name',
  lastName: 'last name',
  avatar: 'a url to an avatar'
}`

const Row = (props) => {
  const { call, request, classes, enabled } = props

  const { error, isLoading, response, runTests } = withTests([request], (input) => {
    const result = {
      endpoint: request.endpoint,
      options: {
        method: request.method,
      }
    }
    if (request.method === 'GET') {
      return result;
    }
    return {...result, options: {
      method: request.method,
      body: JSON.stringify(request.body),
    }}
  }, (input, output) => {
    return true
  }, (input, output) => {
    return request.code === 200
  }, (input, error) => {
    return error.status === request.code
  }, enabled)

  const status = error ? error.status : !!response ? 200 : 0
  const isSet = status === request.code

  let display = 'Success'
  if (isLoading) {
    display = 'Loading'
  } else if (status !== request.code) {
    display = 'Not Correct, Retry?'
  }
  return (
    <TableRow>
      <TableCell>{call.display}</TableCell>
      <TableCell>{request.endpoint}</TableCell>
      <TableCell>{request.method}</TableCell>
      <TableCell><Code className={classes.code}>{JSON.stringify(request.body, null, " ")}</Code></TableCell>
      <TableCell>{`${request.code}`}</TableCell>
      <TableCell><Button className={isSet ? classes.set : classes.error} onClick={runTests}>{display}</Button></TableCell>
    </TableRow>
  )
}
const StyledRow = withStyles(rowStyles)(Row)

const Assignments = (props) => {
  const { classes, section } = props
  let graphFunctions = section.children[0]

  const randomUser = () => {
    const gender = faker.name.gender()
    return {
      type: faker.name.jobType(),
      gender: 'M',
      age: faker.random.number({ min: 0, max: 90 }),
      name: faker.name.firstName(gender),
      username: faker.internet.userName(),
      lastName: faker.name.lastName(gender),
      avatar: faker.internet.avatar()
    }
  }

  const toBeUpdated = {...randomUser(), id: "5e5fc74ff5511f8590f4c3a6"}
  const toBeDeleted = {...randomUser(), id: "5e5fc74ff5511f8590f4c3a5"}
  const users = [[toBeDeleted, toBeUpdated]]

  const { error, isLoading, runTests } = withTests(users, (input) => {
    return {
      endpoint: '/assignments/lecture6/users/setup',
      options: {
        method: 'POST',
        body: JSON.stringify(input),
      }
    }
  }, (input, output) => {
    return _.isEqual(input, output)
  }, (input, output) => {
    return output
  }, (input, error) => {})

  const calls = [
    {
      display: 'Fetch all users',
      requests: [
        {
          method: 'GET',
          endpoint: '/assignments/lecture6/users',
          code: 200,
        }
      ]
    },
    {
      display: 'Create User',
      requests: [
        {
          method: 'POST',
          endpoint: '/assignments/lecture6/users',
          code: 200,
          body: randomUser()
        },
        {
          method: 'POST',
          endpoint: '/assignments/lecture6/users',
          code: 400,
          body: _.omit(randomUser(), "name")
        },
        {
          method: 'POST',
          endpoint: '/assignments/lecture6/users',
          code: 400,
          body: {...randomUser(), age: 200}
        },
        {
          method: 'POST',
          endpoint: '/assignments/lecture6/users',
          code: 400,
          body: {...randomUser(), gender: 'Dinosaur'}
        },
        {
          method: 'POST',
          endpoint: '/assignments/lecture6/users',
          code: 400,
          body: {...randomUser(), age: 'Anotated'}
        },
        {
          method: 'POST',
          endpoint: '/assignments/lecture6/users',
          code: 400,
          body: []
        }
      ]
    },
    {
      display: 'Update User',
      requests: [
        {
          method: 'PUT',
          endpoint: '/assignments/lecture6/users/someId',
          code: 404,
          body: randomUser()
        },
        {
          method: 'PUT',
          endpoint: `/assignments/lecture6/users/${toBeUpdated.id}`,
          code: 400,
          body: []
        },
        {
          method: 'PUT',
          endpoint: `/assignments/lecture6/users/${toBeUpdated.id}`,
          code: 200,
          body: {...toBeUpdated, name: 'Updated'}
        }
      ]
    },
    {
      display: 'Delete User',
      requests: [
        {
          method: 'DELETE',
          endpoint: '/assignments/lecture6/users/someId',
          code: 404,
          body: randomUser()
        },
        {
          method: 'DELETE',
          endpoint: `/assignments/lecture6/users/${toBeDeleted.id}`,
          code: 200,
          body: toBeDeleted
        }
      ]
    }
  ]

  const isSet = !error && !isLoading
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Home Assignments
        <Divider />
      </Typography>
      <Typography>
        This is the same assignment as the one a week before, but now it is required that you have the routes use the Redis Cache API, as well as make them async actions.
        <Code>
          {routes}
        </Code>
      </Typography>
      <Typography id={graphFunctions.id} variant={'title'}>
        {graphFunctions.display}
      </Typography>
      <Typography variant='p'>
        Title: "Implement add/update/delete on a User List on Redis!"<br />
        The user model:
        <Code>
          {model}
        </Code>
        Make sure that before you begin, you accept an array of users to setup the API so that it starts with
        <Code>
          {JSON.stringify(users[0], null, " ")}
        </Code>
        Also the Backend API should be done like this:
        <ol>
          <li><Bold>Using Caching</Bold>. Cache the data (Redis or Normal), invalidate caches on updates!</li>
          <li><Bold>Using Hibernate to Validate</Bold>. Validate Constraints via Hybernate!</li>
        </ol>
        <Typography className={classes.setup}>
          Current implementation is: {!!error ? "false" : "true"}
          <ErrorBox message={error && (error.message || 'Something wrong happened! Check that the server is up and running')} onRetryClicked={runTests} />
          <LoadingIndicator show={isLoading}/>
        </Typography>
      </Typography>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Api</TableCell>
            <TableCell>Method</TableCell>
            <TableCell>Input</TableCell>
            <TableCell>Expected Result</TableCell>
            <TableCell>Retry?</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {calls.map((next, index) => {
            const requests = next.requests || []
            return requests.map((request, cell) => {
              return (
                <StyledRow enabled={isSet} key={`call-${index}-${cell}`} request={request} call={next} />
              )
          })})}
        </TableBody>
      </Table>
    </Fragment>
  )
}

export default withStyles(styles)(Assignments)
