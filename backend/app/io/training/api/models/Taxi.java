package io.training.api.models;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;


@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public @Data class Taxi extends BaseModel{
	private String name;
	private String company;
	private int nrSeats;
}
