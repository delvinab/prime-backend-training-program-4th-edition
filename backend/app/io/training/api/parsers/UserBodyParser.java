package io.training.api.parsers;

/**
 * Created by agonlohaj on 24 Aug, 2020
 */

import akka.util.ByteString;
import com.fasterxml.jackson.databind.JsonNode;
import io.training.api.models.User;
import play.mvc.Result;
import play.libs.F;
import play.libs.streams.Accumulator;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Results;

import javax.inject.Inject;
import java.util.concurrent.Executor;

public class UserBodyParser implements BodyParser<User> {
	@Inject
	private BodyParser.Json jsonParser;
	@Inject
	private Executor executor;

	@Override
	public Accumulator<ByteString, F.Either<play.mvc.Result, User>> apply(Http.RequestHeader request) {
		Accumulator<ByteString, F.Either<Result, JsonNode>> jsonAccumulator = jsonParser.apply(request);
		return jsonAccumulator.map(
				resultOrJson -> {
					if (resultOrJson.left.isPresent()) {
						return F.Either.Left(resultOrJson.left.get());
					}
					JsonNode json = resultOrJson.right.get();
					try {
						User user = play.libs.Json.fromJson(json, User.class);
						return F.Either.Right(user);
					} catch (Exception e) {
						return F.Either.Left(
								Results.badRequest("Unable to read User from json: " + e.getMessage()));
					}
				},
				executor);
	}
}
