package io.training.api.controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.Inject;
import io.training.api.models.User;
import play.cache.NamedCache;
import play.cache.SyncCacheApi;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AssignmentLectureSixController extends Controller {
	@Inject
	@NamedCache("training")
	private SyncCacheApi cacheApi;

	@BodyParser.Of(BodyParser.Json.class)
	public Result setup (Http.Request request) {
		ArrayNode body = (ArrayNode) request.body().asJson();
		List<User> list = this.from(body);
		cacheApi.set("users", list);
		return ok(Json.toJson(list));
	}

	public Result all (Http.Request request) {
		Optional<List<User>> list = cacheApi.getOptional("users");
		return ok(Json.toJson(list.orElse(new ArrayList<>())));
	}

	public List<User> from (ArrayNode users) {
		return StreamSupport
				.stream(users.spliterator(), true)
				.map(next -> Json.fromJson(next, User.class))
				.collect(Collectors.toList());
	}
}