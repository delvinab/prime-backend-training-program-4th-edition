package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.mongodb.client.model.Filters;
import io.training.api.models.Taxi;
import io.training.api.mongo.IMongoDB;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import io.swagger.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Api(value = "Lecture Eleven controllers")
public class LectureElevenController extends Controller {
	@Inject
	IMongoDB mongoDB;

	@ApiOperation(value = "Return All Taxis", response = Taxi.class, responseContainer = "List")
	public Result all(Http.Request request) {
		List<Taxi> taxis = mongoDB.getMongoDatabase().getCollection("taxi", Taxi.class).find().into(new ArrayList<>());
		return ok(Json.toJson(taxis));
	}


	@BodyParser.Of(BodyParser.Json.class)
	@ApiOperation(value = "Store a Taxi", response = Taxi.class)
	@ApiImplicitParams({
		@ApiImplicitParam(name = "A taxi to store", dataType = "io.training.api.models.Taxi", dataTypeClass = Taxi.class, paramType = "body")
	})
	public Result save (Http.Request request) {
		JsonNode node = request.body().asJson();
		Taxi taxi = Json.fromJson(node, Taxi.class);
		// Store with ID
		taxi.setId(new ObjectId());
		mongoDB.getMongoDatabase()
				.getCollection("pojo", Taxi.class)
				.insertOne(taxi);

		return ok(Json.toJson(taxi));
	}


	@ApiOperation(value = "Removes a Taxi", response = Taxi.class)
	@ApiImplicitParams({@ApiImplicitParam(name = "id", dataType = "String", paramType = "query")})
	public Result remove (String id, Http.Request request) {
		if (!ObjectId.isValid(id)) {
			return badRequest("Invalid Parameters Supplied");
		}
		Taxi taxi = mongoDB.getMongoDatabase()
				.getCollection("pojo", Taxi.class)
				.find(Filters.eq("_id", new ObjectId(id)))
				.first();
		if (taxi == null) {
			return notFound("The taxi was not found");
		}
		mongoDB.getMongoDatabase()
				.getCollection("pojo", Taxi.class)
				.deleteOne(Filters.eq("_id", taxi.getId()));

		return ok(Json.toJson(taxi));
	}
}